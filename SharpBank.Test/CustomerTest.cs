﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public String TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

             //mark / delete this:
            //Assert.AreEqual("Statement for Henry\n" +
            //        "\n" +
            //        "Checking Account\n" +
            //        "  deposit $100.00\n" +
            //        "Total $100.00\n" +
            //        "\n" +
            //        "Savings Account\n" +
            //        "  deposit $4,000.00\n" +
            //        "  withdrawal $200.00\n" +
            //        "Total $3,800.00\n" +
            //        "\n" +
            //        "Total In All Accounts $3,900.00", henry.GetStatement());

             TranferBetweenAccounts(checkingAccount, savingsAccount, 100);

            //return GetStatement
            return henry.GetStatement();
        }
        
      

        [Test]
        public void TranferBetweenAccounts(Account a, Account b, double amount)
        {
            a.Deposit(amount);
            b.Withdraw(amount);
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            oscar.OpenAccount(new Account(Account.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }
    }
}
