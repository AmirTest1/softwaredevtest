﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        //change it to public and add readonly
        public readonly DateTime transactionDate;


        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }

    }
}
